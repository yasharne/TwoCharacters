import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by yashar on 12/5/16.
 */
public class TwoCharacters {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        char chars[] = in.next().toCharArray();
        Map<Integer, Character> m = new HashMap<Integer, Character>();
        int counter = 0;
        for (int i = 0; i < chars.length; i++) {
//            System.out.println(chars[i]);
            if (!m.containsValue(chars[i])) {
                m.put(counter, chars[i]);
                counter++;
            }

        }

//        for (Map.Entry<Integer, Character> entry : m.entrySet()) {
//            System.out.println(entry.getKey()+" : "+entry.getValue());
//        }
//        for (Integer name: m.keySet()){
//
//            String key =name.toString();
//            String value = m.get(name).toString();
//            System.out.println(key + " " + value);
//
//
//        }
//        System.out.println(m.size());
        if (m.size() == 1){
            System.out.println(0);
//            System.exit(0);
        }
        else if (m.size() == 2){
            if (isOk(chars)){
                System.out.println(chars.length);
            }else{
                System.out.println(0);
            }
        }else {
            String tmp_string;
            String string = "";
//            System.out.println(m.size());
            for (int i = 0; i < m.size(); i++) {
                for (int j = 1; j < m.size(); j++) {
                    int tmp = 0;
                    tmp_string = new String(chars);
//                    System.out.println(tmp_string);
                    for (tmp = 0; tmp < m.size(); tmp++) {
                        if (tmp != i && tmp != j){
                            tmp_string = tmp_string.replaceAll(m.get(tmp).toString(), "");
//                            System.out.println("Get: "  + m.get(tmp).toString());
//                            System.out.println("After: " + tmp_string);
                        }
                    }
                    if (isOk(tmp_string.toCharArray())){
                        if (tmp_string.length() > string.length()){
                            string = tmp_string;
                        }
                    }
                }
            }
            System.out.println(string.length());
        }


    }

    public static boolean isOk(char[] s)
    {
        for(int i=1;i<s.length;i++)
        {
            if(s[i] == s[i - 1])
                return false;
        }
        return true;
    }
}
